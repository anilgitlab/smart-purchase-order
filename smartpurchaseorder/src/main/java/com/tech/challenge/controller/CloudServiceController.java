package com.tech.challenge.controller;
/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 7, 2016        AKurmi				Created
*/

/***************************************************************/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tech.challenge.dao.CloudServiceDAO;
import com.tech.challenge.service.SubscriptionCreatorService;
import com.tech.challenge.som.CloudServices;
import com.tech.challenge.som.Orders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***************************************************************/

/**
 * CloudServiceController
 */
@RestController
@RequestMapping("api/v1/services")
public class CloudServiceController
{
    private static final Logger        LOGGER = LoggerFactory
            .getLogger(CloudServiceController.class);

    @Autowired
    @Qualifier("cloudServieDAO")
    private CloudServiceDAO            serviceDAO;

    @Autowired
    private SubscriptionCreatorService subCreator;

    /**
     * 
     * getAllServices
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public CloudServices getAllServices()
    {
        return serviceDAO.allAvailabeServices();
    }

    /**
     * 
     * subscribe
     *
     * @param services
     */
    @RequestMapping(value = "/subscribe", method = RequestMethod.POST, consumes = "application/json")
    public void subscribe(@RequestBody CloudServices services)
    {
        Orders orders = subCreator.createSubscription(services);
        subCreator.callSubscriptionAPI(orders);

    }

    /**
     * 
     * save
     *
     * @param services
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> save(@RequestBody CloudServices services)
    {
        serviceDAO.save(services);

        return new ResponseEntity<String>("saved.", HttpStatus.OK);

    }

    /**
     * Generic exception handler.
     * 
     * @param ex
     * @return
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> handleAppException(Exception ex)
    {
        return new ResponseEntity<String>("NS Mobile Error : ",
                HttpStatus.NOT_IMPLEMENTED);
    }

    /**
     * 
     * fallbackMethod
     *
     * @return
     */
    @RequestMapping("*")
    public ResponseEntity<Object> fallbackMethod()
    {
        LOGGER.info("invalid URL");
        return new ResponseEntity<Object>("NS Mobile Error : ",
                HttpStatus.NOT_IMPLEMENTED);
    }

}
