package com.tech.challenge.controller;
/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 8, 2016        akurmi				Created
*/

/***************************************************************/
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tech.challenge.service.ImageProviderService;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

/***************************************************************/

/**
 * 
 */
@RestController
@RequestMapping("api/v1/image")
public class ImageProviderController
{
    @Autowired
    private ImageProviderService imageService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> testphoto(@PathVariable("id") String id)
            throws IOException
    {

        byte[] data = imageService.getImage(id);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);

        return new ResponseEntity<byte[]>(data, headers, HttpStatus.CREATED);
    }

}
