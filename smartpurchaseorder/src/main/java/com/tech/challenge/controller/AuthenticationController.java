package com.tech.challenge.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

/*Copyright anil kurmi 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tech.challenge.service.AuthenticationService;
import com.tech.challenge.service.PasswordEncorderService;

/***************************************************************/

/**
 * 
 */
@RestController
@RequestMapping("api/v1/user")
public class AuthenticationController
{
    @Autowired
    private AuthenticationService   authService;

    @Autowired
    private PasswordEncorderService passwordService;

    @RequestMapping(value = { "/authenticate" }, method = {
            RequestMethod.POST })
    public void authenticate(@RequestBody User user, HttpServletRequest request,
            HttpSession session, Locale locale)
    {
        authService.authenticate(user.getUserName(),
                passwordService.encodePassword(user.getPassword()));
    }

    @RequestMapping(value = { "/logout" }, method = { RequestMethod.POST })
    public void logout(

            HttpServletRequest request, HttpSession session)
    {
        this.authService.logout();
        
        session.invalidate();

    }

}

class User
{
    String userName;

    String password;

    /**
     * Get userName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * Set userName
     *
     * @param String
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * Get password
     *
     * @return String
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Set password
     *
     * @param String
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

}