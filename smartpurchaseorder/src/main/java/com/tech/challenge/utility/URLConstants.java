package com.tech.challenge.utility;
/*Copyright ABS Nautical Systems 2002-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 7, 2016        AKurmi				Created
*/

/***************************************************************/

/***************************************************************/

/**
 * 
 */
public interface URLConstants
{
    String BASE_URL     = " www.appdirect.com";

    String GET_PO       = "/api/billing/v1/orders/{orderId}";

    String POs          = "/api/billing/v1/companies/{companyId}/orders";

    String POsUSER      = "/api/billing/v1/companies/{companyId}/users/{userId}/orders";

    String POsSubscribe = "/api/billing/v1/subscriptions/{subscriptionId}/orders";

    String CREATE_SUBS  = "/api/billing/v1/companies/{companyId}/users/{userId}/subscriptions";

    String SUBS_COMPANY = "/api/billing/v1/companies/<companyId>/subscriptions";

    String SUBS_USER    = "/api/billing/v1/companies/<companyId>/users/<userId>/subscriptions";

    String GET_SUBS     = "/api/billing/v1/subscriptions/<subscriptionId>";

    String CHANGE_SUBS  = "/api/billing/v1/companies/{companyId}/users/{userId}/subscriptions/{subscriptionId}";

    String CANCEL_SUBS  = "/api/billing/v1/subscriptions/{subscriptionId}";
}
