package com.tech.challenge.integration;
/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 8, 2016        AKurmi				Created
*/

/***************************************************************/
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/***************************************************************/

/**
 * <p>
 * This class {@code BaseIntegration} contains headers to authenticate using
 * OAuth 1.0 protocol , OAuth oath_consumer_key has been obtaned from AppDirect
 * developer account
 * </p>
 * <br>
 * <p>
 * This class contain useful methods to call AppDirect API.
 * </p>
 */
public abstract class BaseIntegration
{

    protected RestTemplate        rt      = new RestTemplate();

    protected Map<String, String> vars    = new HashMap<>();

    HttpHeaders                   headers = new HttpHeaders();

    String                        token   = "OAuth oauth_consumer_key=\"smartpurchaseorder-140726\","
            + "oauth_signature_method=\"HMAC-SHA1\","
            + "oauth_timestamp=\"1478539464\",oauth_nonce=\"sBLvwz\","
            + "oauth_version=\"1.0\",oauth_signature=\"Q5AwkE9llkfVx6ujU1aou5P1fuo%3D\"";

    /**
     * 
     * Constructor BaseIntegration
     *
     */
    public BaseIntegration()
    {
        rt.getMessageConverters()
                .add(new MappingJackson2HttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());

        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("Authorization", token);
    }

    /**
     * 
     * toJson
     *
     * @param obj
     * @return
     */
    public String toJson(Object obj)
    {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try
        {
            jsonInString = mapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }

        return jsonInString;
    }

}
