package com.tech.challenge.integration;
/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.tech.challenge.som.Order;
import com.tech.challenge.som.Subscription;
import com.tech.challenge.utility.URLConstants;

/***************************************************************/

/**
 * <p>
 * This class {@code SubscriptionIntegration} contains all the
 * methods related to subscription integrated with APPDIRECT api.
 * </p>
 * <br>
 * <p>
 * 
 * <li>createSubscription</li>
 * 
 * <li>changeSubscription</li>
 * 
 * <li>cancelSubscription</li>
 * 
 */
@Component
public class SubscriptionIntegration extends BaseIntegration
{

    /**
     * 
     * createSubscription
     *
     * @param customerId
     * @param userId
     * @param order
     * @return
     */
    public Subscription createSubscription(long customerId, long userId,
            Order order)
    {
        final HttpEntity<Order> entity = new HttpEntity<Order>(order, headers);

        ResponseEntity<Subscription> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.CREATE_SUBS,
                HttpMethod.POST, entity, Subscription.class, customerId,
                userId);

        return pos.getBody();
    }

    /**
     * 
     * changeSubscription
     *
     * @param customerId
     * @param userId
     * @param subscription
     * @return
     */
    public Subscription changeSubscription(long customerId, long userId,
            Subscription subscription)
    {

        final HttpEntity<Subscription> entity = new HttpEntity<Subscription>(
                subscription, headers);

        ResponseEntity<Subscription> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.CHANGE_SUBS,
                HttpMethod.PUT, entity, Subscription.class, customerId, userId,
                subscription);

        return pos.getBody();

    }

    /**
     * 
     * cancelSubscription
     *
     * @param customerId
     * @param userId
     * @param subscriptionId
     * @return
     */
    public String cancelSubscription(long customerId, long userId,
            long subscriptionId)
    {

        final HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.CANCEL_SUBS,
                HttpMethod.DELETE, entity, String.class, customerId, userId,
                subscriptionId);

        return pos.getBody();

    }

}
