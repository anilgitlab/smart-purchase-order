package com.tech.challenge.integration;

/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

import com.tech.challenge.som.PurchaseOrder;
import com.tech.challenge.som.PurchaseOrders;
import com.tech.challenge.utility.URLConstants;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/***************************************************************/

/**
 * PurchaseOrderIntegration
 */
@Component
public class PurchaseOrderIntegration extends BaseIntegration
{

    /**
     * 
     * getPO
     *
     * @param poId
     * @return
     */
    public PurchaseOrder getPO(long poId)
    {
        final HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<PurchaseOrder> po = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.GET_PO, HttpMethod.GET,
                entity, PurchaseOrder.class, poId);

        return po.getBody();
    }

    /**
     * 
     * getAllPOForCompany
     *
     * @param companyId
     * @return
     */
    public PurchaseOrders getAllPOForCompany(long companyId)
    {

        final HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<PurchaseOrders> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.POs, HttpMethod.GET,
                entity, PurchaseOrders.class, companyId);

        return pos.getBody();
    }

    /**
     * 
     * getAllPOForUser
     *
     * @param companyId
     * @param userId
     * @return
     */
    public PurchaseOrders getAllPOForUser(long companyId, long userId)
    {

        final HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<PurchaseOrders> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.POsUSER, HttpMethod.GET,
                entity, PurchaseOrders.class, companyId, userId);

        return pos.getBody();
    }

    /**
     * 
     * getAllPOSubscribed
     *
     * @param subscriptionId
     * @return
     */
    public PurchaseOrders getAllPOSubscribed(long subscriptionId)
    {

        final HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<PurchaseOrders> pos = this.rt.exchange(
                URLConstants.BASE_URL + URLConstants.POsSubscribe,
                HttpMethod.GET, entity, PurchaseOrders.class, subscriptionId);

        return pos.getBody();
    }

}
