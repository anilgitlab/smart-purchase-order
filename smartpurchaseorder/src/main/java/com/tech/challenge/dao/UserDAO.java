package com.tech.challenge.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.tech.challenge.service.PasswordEncorderService;

public class UserDAO
{
    private JdbcTemplate            jdbcTemplateObject;

    @Autowired
    private PasswordEncorderService passwordService;

    public void setDataSource(DataSource dataSource)
    {
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public boolean authenticate(String userName, String password)
    {
        String decodPassword = passwordService.decodePassword(password);

        return true;
    }

    public void loadAuthorizeData(long userid)
    {
        String query = "";

        jdbcTemplateObject.query(query, Arrays.asList(userid).toArray(),
                new UserAccess());

    }

}

class UserAccess implements RowMapper<Object>
{

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return null;
    }
}
