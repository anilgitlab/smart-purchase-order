package com.tech.challenge.dao;
/*Copyright anil 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 7, 2016        AKurmi				Created
*/

/***************************************************************/
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.tech.challenge.som.CloudService;
import com.tech.challenge.som.CloudServices;

/***************************************************************/

/**
 * CloudServiceDAO
 */
public class CloudServiceDAO
{
    private JdbcTemplate jdbcTemplateObject;

    /**
     * 
     * setDataSource
     *
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource)
    {
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    /**
     * 
     * allAvailabeServices
     *
     * @return
     */
    public CloudServices allAvailabeServices()
    {
        String SQL = "select * from cloudservice";

        List<CloudService> serviceList = jdbcTemplateObject.query(SQL,
                new CloudServiceMapper());
        CloudServices services = new CloudServices();
        services.setCloudServices(serviceList);
        return services;
    }

    /**
     * save
     *
     * @param services
     */
    public void save(CloudServices services)
    {
        
    }

}


class CloudServiceMapper implements RowMapper<CloudService>
{

    public CloudService mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        CloudService service = new CloudService();

        service.setServiceId(rs.getLong("serviceId"));
        service.setServiceName(rs.getString("serviceName"));
        service.setVendorName(rs.getString("vendorName"));

        return service;
    }
}
