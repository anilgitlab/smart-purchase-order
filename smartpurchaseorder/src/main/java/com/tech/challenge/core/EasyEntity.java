package com.tech.challenge.core;
/*Copyright anil kurmi 2012-2016*/


/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/

/***************************************************************/

/**
 * {@code EasyEntity} class is
 * parent entity for all the entity classes to interact with Database
 * <p>
 * provide all the userful abstract medhods related to DB opeations
 * </p>
 * <br>
 */
public abstract class EasyEntity
{
    private int    id;

    private String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return this.id;
    }

    /**
     * return primary key
     * getPK
     *
     * @return
     */
    abstract public long getPK();

}
