package com.tech.challenge.core;

/*Copyright anil kurmi 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/
import java.util.List;

/***************************************************************/

/**
 * <p>
 * Interface {@code IDAO} contain all the methods related to database operations
 * </p>
 * 
 */
public interface IDAO
{
    /**
     * 
     * create
     *
     * @param entity
     */
    public void create(EasyEntity entity);

    /**
     * 
     * delete
     *
     * @param entity
     * @return
     */
    public boolean delete(EasyEntity entity);

    /**
     * 
     * update
     *
     * @param entity
     * @return
     */
    public boolean update(EasyEntity entity);

    /**
     * 
     * getAll
     *
     * @return
     */
    public List<EasyEntity> getAll();

}
