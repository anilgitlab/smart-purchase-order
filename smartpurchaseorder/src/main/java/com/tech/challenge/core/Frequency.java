package com.tech.challenge.core;

public enum Frequency
{
    ONE_TIME, MONTHLY, QUATERLY, SIX_MONTHS, YEARLY, TWO_YEARS, DAILY;

}
