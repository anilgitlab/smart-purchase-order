package com.tech.challenge.core;

/**
 * 
 */
public enum Currency
{
    USD, CAD, EUR, JPY, GBP, KRW, CHF, SEK, SGD, MYR, AUD;
}
