package com.tech.challenge.som;

import java.util.Date;

import com.tech.challenge.core.Status;

public class Subscription
{
    private long      id;

    private Date      creationDate;

    private long      externalAccountId;

    private Status    status;

    private int       maxUsers;

    private Order     order;

    private long      paymentPlanId;

    private long      companyId;

    private OrderLine orderLine;

    /**
     * Get id
     *
     * @return long
     */
    public long getId()
    {
        return id;
    }

    /**
     * Set id
     *
     * @param long
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Get creationDate
     *
     * @return Date
     */
    public Date getCreationDate()
    {
        return creationDate;
    }

    /**
     * Set creationDate
     *
     * @param Date
     */
    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    /**
     * Get externalAccountId
     *
     * @return long
     */
    public long getExternalAccountId()
    {
        return externalAccountId;
    }

    /**
     * Set externalAccountId
     *
     * @param long
     */
    public void setExternalAccountId(long externalAccountId)
    {
        this.externalAccountId = externalAccountId;
    }

    /**
     * Get status
     *
     * @return Status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * Set status
     *
     * @param Status
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }

    /**
     * Get maxUsers
     *
     * @return int
     */
    public int getMaxUsers()
    {
        return maxUsers;
    }

    /**
     * Set maxUsers
     *
     * @param int
     */
    public void setMaxUsers(int maxUsers)
    {
        this.maxUsers = maxUsers;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public Order getOrder()
    {
        return order;
    }

    /**
     * Set order
     *
     * @param Order
     */
    public void setOrder(Order order)
    {
        this.order = order;
    }

    /**
     * Get paymentPlanId
     *
     * @return long
     */
    public long getPaymentPlanId()
    {
        return paymentPlanId;
    }

    /**
     * Set paymentPlanId
     *
     * @param long
     */
    public void setPaymentPlanId(long paymentPlanId)
    {
        this.paymentPlanId = paymentPlanId;
    }

    /**
     * Get companyId
     *
     * @return long
     */
    public long getCompanyId()
    {
        return companyId;
    }

    /**
     * Set companyId
     *
     * @param long
     */
    public void setCompanyId(long companyId)
    {
        this.companyId = companyId;
    }

    /**
     * Get orderLine
     *
     * @return OrderLine
     */
    public OrderLine getOrderLine()
    {
        return orderLine;
    }

    /**
     * Set orderLine
     *
     * @param OrderLine
     */
    public void setOrderLine(OrderLine orderLine)
    {
        this.orderLine = orderLine;
    }

}
