package com.tech.challenge.som;

import java.util.ArrayList;
import java.util.List;

public class Orders
{
    private List<Order> orders = new ArrayList<>();

    /**
     * Get orders
     *
     * @return List<Order>
     */
    public List<Order> getOrders()
    {
        return orders;
    }

    /**
     * Set orders
     *
     * @param List<Order>
     */
    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

    /**
     * addOrder
     *
     * @param cloudServiceToSubscriptionMapping
     */
    public void addOrder(Order order)
    {
        this.orders.add(order);

    }

}
