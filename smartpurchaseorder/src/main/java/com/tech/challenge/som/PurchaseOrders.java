package com.tech.challenge.som;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class PurchaseOrders
{
    private List<PurchaseOrder> orders = new ArrayList<>();

    public List<PurchaseOrder> getOrders()
    {
        return orders;
    }

    public void setOrders(List<PurchaseOrder> orders)
    {
        this.orders = orders;
    }

}
