package com.tech.challenge.som;
/*Copyright anil kurmi 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/

/***************************************************************/

/**
 * 
 */
public class Customer
{

    private long   customerId;

    private String customerName;

    private String address;

    private String eamil;

    /**
     * Get customerId
     *
     * @return long
     */
    public long getCustomerId()
    {
        return customerId;
    }

    /**
     * Set customerId
     *
     * @param long
     */
    public void setCustomerId(long customerId)
    {
        this.customerId = customerId;
    }

    /**
     * Get customerName
     *
     * @return String
     */
    public String getCustomerName()
    {
        return customerName;
    }

    /**
     * Set customerName
     *
     * @param String
     */
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    /**
     * Get address
     *
     * @return String
     */
    public String getAddress()
    {
        return address;
    }

    /**
     * Set address
     *
     * @param String
     */
    public void setAddress(String address)
    {
        this.address = address;
    }

    /**
     * Get eamil
     *
     * @return String
     */
    public String getEamil()
    {
        return eamil;
    }

    /**
     * Set eamil
     *
     * @param String
     */
    public void setEamil(String eamil)
    {
        this.eamil = eamil;
    }

}
