package com.tech.challenge.som;

import java.util.ArrayList;
import java.util.List;

public class Subscriptions
{

    private List<Subscription> subscriptions = new ArrayList<>();

    /**
     * Get subscriptions
     *
     * @return List<Subscription>
     */
    public List<Subscription> getSubscriptions()
    {
        return subscriptions;
    }

    /**
     * Set subscriptions
     *
     * @param List<Subscription>
     */
    public void setSubscriptions(List<Subscription> subscriptions)
    {
        this.subscriptions = subscriptions;
    }

}
