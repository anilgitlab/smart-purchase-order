package com.tech.challenge.som;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CloudService
{
    @JsonProperty
    private long                serviceId;

    @JsonProperty
    private String              serviceName;

    @JsonProperty
    private String              vendorName;

    @JsonProperty
    private int                 quantity;

    @JsonProperty
    private List<CloudService>  association;

    @JsonProperty
    private Map<String, String> properties;

    /**
     * Get serviceId
     *
     * @return long
     */
    public long getServiceId()
    {
        return serviceId;
    }

    /**
     * Set serviceId
     *
     * @param long
     */
    public void setServiceId(long serviceId)
    {
        this.serviceId = serviceId;
    }

    /**
     * Get serviceName
     *
     * @return String
     */
    public String getServiceName()
    {
        return serviceName;
    }

    /**
     * Set serviceName
     *
     * @param String
     */
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    /**
     * Get vendorName
     *
     * @return String
     */
    public String getVendorName()
    {
        return vendorName;
    }

    /**
     * Set vendorName
     *
     * @param String
     */
    public void setVendorName(String vendorName)
    {
        this.vendorName = vendorName;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public int getQuantity()
    {
        return quantity;
    }

    /**
     * Set quantity
     *
     * @param int
     */
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    /**
     * Get association
     *
     * @return List<CloudService>
     */
    public List<CloudService> getAssociation()
    {
        return association;
    }

    /**
     * Set association
     *
     * @param List<CloudService>
     */
    public void setAssociation(List<CloudService> association)
    {
        this.association = association;
    }

    /**
     * Get properties
     *
     * @return Map<String,String>
     */
    public Map<String, String> getProperties()
    {
        return properties;
    }

    /**
     * Set properties
     *
     * @param Map<String,String>
     */
    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }

}
