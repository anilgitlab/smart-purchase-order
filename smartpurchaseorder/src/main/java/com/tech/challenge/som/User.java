package com.tech.challenge.som;

public class User
{
    String userName;

    long   userId;

    String email;

    String password;

    /**
     * Constructor User
     *
     * @param string
     * @param string2
     */
    public User(String string, String string2)
    {
        this.userName = string;
        this.password = string2;
    }

    /**
     * Get userName
     *
     * @return String
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * Set userName
     *
     * @param String
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * Get userId
     *
     * @return long
     */
    public long getUserId()
    {
        return userId;
    }

    /**
     * Set userId
     *
     * @param long
     */
    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    /**
     * Get email
     *
     * @return String
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Set email
     *
     * @param String
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * Get password
     *
     * @return String
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Set password
     *
     * @param String
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

}
