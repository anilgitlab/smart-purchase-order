package com.tech.challenge.som;

import java.util.Date;
import java.util.Map;

import com.tech.challenge.core.Currency;
import com.tech.challenge.core.DocumentType;
import com.tech.challenge.core.Frequency;
import com.tech.challenge.core.Status;

public class PurchaseOrder
{
    private long                id;

    private Date                startDate;

    private Date                endDate;

    private Date                nextBillingDate;

    private Status              staus;

    private Frequency           frequency;

    private Currency            currency;

    private DocumentType        type;

    private long                totalPrice;

    private long                userid;

    private long                companyid;

    private String              salesSupport;

    private long                previousOrder;

    private long                nextOrder;

    private long                paymentPlan;

    private long                discount;

    private long                contract;

    private int                 oneTimeOrders;

    private int                 orderLines;

    private Map<String, String> parameters;

    /**
     * Get id
     *
     * @return long
     */
    public long getId()
    {
        return id;
    }

    /**
     * Set id
     *
     * @param long
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Get startDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * Set startDate
     *
     * @param Date
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Get endDate
     *
     * @return Date
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * Set endDate
     *
     * @param Date
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * Get nextBillingDate
     *
     * @return Date
     */
    public Date getNextBillingDate()
    {
        return nextBillingDate;
    }

    /**
     * Set nextBillingDate
     *
     * @param Date
     */
    public void setNextBillingDate(Date nextBillingDate)
    {
        this.nextBillingDate = nextBillingDate;
    }

    /**
     * Get staus
     *
     * @return Status
     */
    public Status getStaus()
    {
        return staus;
    }

    /**
     * Set staus
     *
     * @param Status
     */
    public void setStaus(Status staus)
    {
        this.staus = staus;
    }

    /**
     * Get frequency
     *
     * @return Frequency
     */
    public Frequency getFrequency()
    {
        return frequency;
    }

    /**
     * Set frequency
     *
     * @param Frequency
     */
    public void setFrequency(Frequency frequency)
    {
        this.frequency = frequency;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public Currency getCurrency()
    {
        return currency;
    }

    /**
     * Set currency
     *
     * @param Currency
     */
    public void setCurrency(Currency currency)
    {
        this.currency = currency;
    }

    /**
     * Get type
     *
     * @return DocumentType
     */
    public DocumentType getType()
    {
        return type;
    }

    /**
     * Set type
     *
     * @param DocumentType
     */
    public void setType(DocumentType type)
    {
        this.type = type;
    }

    /**
     * Get totalPrice
     *
     * @return long
     */
    public long getTotalPrice()
    {
        return totalPrice;
    }

    /**
     * Set totalPrice
     *
     * @param long
     */
    public void setTotalPrice(long totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    /**
     * Get userid
     *
     * @return long
     */
    public long getUserid()
    {
        return userid;
    }

    /**
     * Set userid
     *
     * @param long
     */
    public void setUserid(long userid)
    {
        this.userid = userid;
    }

    /**
     * Get companyid
     *
     * @return long
     */
    public long getCompanyid()
    {
        return companyid;
    }

    /**
     * Set companyid
     *
     * @param long
     */
    public void setCompanyid(long companyid)
    {
        this.companyid = companyid;
    }

    /**
     * Get salesSupport
     *
     * @return String
     */
    public String getSalesSupport()
    {
        return salesSupport;
    }

    /**
     * Set salesSupport
     *
     * @param String
     */
    public void setSalesSupport(String salesSupport)
    {
        this.salesSupport = salesSupport;
    }

    /**
     * Get previousOrder
     *
     * @return long
     */
    public long getPreviousOrder()
    {
        return previousOrder;
    }

    /**
     * Set previousOrder
     *
     * @param long
     */
    public void setPreviousOrder(long previousOrder)
    {
        this.previousOrder = previousOrder;
    }

    /**
     * Get nextOrder
     *
     * @return long
     */
    public long getNextOrder()
    {
        return nextOrder;
    }

    /**
     * Set nextOrder
     *
     * @param long
     */
    public void setNextOrder(long nextOrder)
    {
        this.nextOrder = nextOrder;
    }

    /**
     * Get paymentPlan
     *
     * @return long
     */
    public long getPaymentPlan()
    {
        return paymentPlan;
    }

    /**
     * Set paymentPlan
     *
     * @param long
     */
    public void setPaymentPlan(long paymentPlan)
    {
        this.paymentPlan = paymentPlan;
    }

    /**
     * Get discount
     *
     * @return long
     */
    public long getDiscount()
    {
        return discount;
    }

    /**
     * Set discount
     *
     * @param long
     */
    public void setDiscount(long discount)
    {
        this.discount = discount;
    }

    /**
     * Get contract
     *
     * @return long
     */
    public long getContract()
    {
        return contract;
    }

    /**
     * Set contract
     *
     * @param long
     */
    public void setContract(long contract)
    {
        this.contract = contract;
    }

    /**
     * Get oneTimeOrders
     *
     * @return int
     */
    public int getOneTimeOrders()
    {
        return oneTimeOrders;
    }

    /**
     * Set oneTimeOrders
     *
     * @param int
     */
    public void setOneTimeOrders(int oneTimeOrders)
    {
        this.oneTimeOrders = oneTimeOrders;
    }

    /**
     * Get orderLines
     *
     * @return int
     */
    public int getOrderLines()
    {
        return orderLines;
    }

    /**
     * Set orderLines
     *
     * @param int
     */
    public void setOrderLines(int orderLines)
    {
        this.orderLines = orderLines;
    }

    /**
     * Get parameters
     *
     * @return Map<String,String>
     */
    public Map<String, String> getParameters()
    {
        return parameters;
    }

    /**
     * Set parameters
     *
     * @param Map<String,String>
     */
    public void setParameters(Map<String, String> parameters)
    {
        this.parameters = parameters;
    }

}
