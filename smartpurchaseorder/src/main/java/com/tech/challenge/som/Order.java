package com.tech.challenge.som;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tech.challenge.core.Currency;
import com.tech.challenge.core.DocumentType;
import com.tech.challenge.core.Frequency;
import com.tech.challenge.core.Status;

public class Order
{
    private long            paymentPlanId;

    private Date            startDate;

    private Date            nextBillingDate;

    private Status          status;

    private Frequency       frequency;

    private Currency        currency;

    private DocumentType    type;

    double                  totalPrice;

    User                    user;

    private List<OrderLine> orderLines = new ArrayList<OrderLine>();

    /**
     * Get startDate
     *
     * @return Date
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * Set startDate
     *
     * @param Date
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Get nextBillingDate
     *
     * @return Date
     */
    public Date getNextBillingDate()
    {
        return nextBillingDate;
    }

    /**
     * Set nextBillingDate
     *
     * @param Date
     */
    public void setNextBillingDate(Date nextBillingDate)
    {
        this.nextBillingDate = nextBillingDate;
    }

    /**
     * Get status
     *
     * @return Status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * Set status
     *
     * @param Status
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }

    /**
     * Get frequency
     *
     * @return Frequency
     */
    public Frequency getFrequency()
    {
        return frequency;
    }

    /**
     * Set frequency
     *
     * @param Frequency
     */
    public void setFrequency(Frequency frequency)
    {
        this.frequency = frequency;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public Currency getCurrency()
    {
        return currency;
    }

    /**
     * Set currency
     *
     * @param Currency
     */
    public void setCurrency(Currency currency)
    {
        this.currency = currency;
    }

    /**
     * Get type
     *
     * @return DocumentType
     */
    public DocumentType getType()
    {
        return type;
    }

    /**
     * Set type
     *
     * @param DocumentType
     */
    public void setType(DocumentType type)
    {
        this.type = type;
    }

    /**
     * Get totalPrice
     *
     * @return double
     */
    public double getTotalPrice()
    {
        return totalPrice;
    }

    /**
     * Set totalPrice
     *
     * @param double
     */
    public void setTotalPrice(double totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    /**
     * Get user
     *
     * @return User
     */
    public User getUser()
    {
        return user;
    }

    /**
     * Set user
     *
     * @param User
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Get paymentPlanId
     *
     * @return long
     */
    public long getPaymentPlanId()
    {
        return paymentPlanId;
    }

    /**
     * Set paymentPlanId
     *
     * @param long
     */
    public void setPaymentPlanId(long paymentPlanId)
    {
        this.paymentPlanId = paymentPlanId;
    }

    /**
     * Get orderLines
     *
     * @return List<OrderLine>
     */
    public List<OrderLine> getOrderLines()
    {
        return orderLines;
    }

    /**
     * Set orderLines
     *
     * @param List<OrderLine>
     */
    public void setOrderLines(List<OrderLine> orderLines)
    {
        this.orderLines = orderLines;
    }

}
