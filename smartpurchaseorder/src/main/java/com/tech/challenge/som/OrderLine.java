package com.tech.challenge.som;

public class OrderLine
{

    String unit;

    int    quantity;

    /**
     * Get unit
     *
     * @return String
     */
    public String getUnit()
    {
        return unit;
    }

    /**
     * Set unit
     *
     * @param String
     */
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public int getQuantity()
    {
        return quantity;
    }

    /**
     * Set quantity
     *
     * @param int
     */
    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

}
