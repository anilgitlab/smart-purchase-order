package com.tech.challenge.som;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CloudServices
{

    @JsonProperty
    private List<CloudService> cloudServices = new ArrayList<>();

    /**
     * Get cloudServices
     *
     * @return List<CloudService>
     */
    public List<CloudService> getCloudServices()
    {
        return cloudServices;
    }

    /**
     * Set cloudServices
     *
     * @param List<CloudService>
     */
    public void setCloudServices(List<CloudService> cloudServices)
    {
        this.cloudServices = cloudServices;
    }

}
