package com.tech.challenge.service;
/*Copyright anil kurmi 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tech.challenge.dao.UserDAO;

/***************************************************************/

/**
 * AuthenticationService
 */
@Service
public class AuthenticationService
{
    @Autowired
    private UserContext userContext;

    @Autowired
    private UserDAO     userDAO;

    /**
     * 
     * authenticate
     *
     * @param userName
     * @param password
     * @return
     */
    public boolean authenticate(String userName, String password)
    {
        if (this.userDAO.authenticate(userName, password))
        {
            userContext.populateUserContext();

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 
     * logout
     *
     */
    public void logout()
    {
        this.userContext.clearUserContext();
    }
}
