package com.tech.challenge.service;
/*Copyright anil kurmi 2012-2016*/

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 6, 2016        AKurmi				Created
*/

/***************************************************************/
import org.springframework.beans.factory.annotation.Autowired;

import com.tech.challenge.dao.UserDAO;

/***************************************************************/

/**
 * AuthorizationService
 */
public class AuthorizationService
{

    @Autowired
    private UserContext userContext;
    
    @Autowired
    private UserDAO userDAO;

    /**
     * 
     * populateUserAccess
     *
     */
    public void populateUserAccess()
    {
        //load user access data from database.
        userDAO.loadAuthorizeData(userContext.getUserId());
    }

}
