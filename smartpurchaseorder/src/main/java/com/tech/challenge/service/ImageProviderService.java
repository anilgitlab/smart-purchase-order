package com.tech.challenge.service;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;

@Service
public class ImageProviderService
{

    private final String IMAGE_STORE = "/image/store";

    public byte[] getImage(String id)
    {
        File imgPath = new File(IMAGE_STORE + File.separator + id);
        BufferedImage bufferedImage = null;
        try
        {
            bufferedImage = ImageIO.read(imgPath);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        WritableRaster raster = bufferedImage.getRaster();
        DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

        return (data.getData());

    }

}
