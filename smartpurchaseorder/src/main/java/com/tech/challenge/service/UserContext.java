package com.tech.challenge.service;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.tech.challenge.som.Customer;
import com.tech.challenge.som.User;

/**
 * UserContext
 */
@Component
public class UserContext extends HashMap<String, Object>
{
    private static final long serialVersionUID = 6558476710277138343L;

    public final String       USERID           = "userId";

    public final String       USERNAME         = "userName";

    public final String       CURRENTUSER      = "currentUser";

    public final String       CUSTOMER         = "customer";

    public long getUserId()
    {
        return (long) super.get(USERID);
    }

    public User currentUser()
    {
        return (User) super.get(CURRENTUSER);
    }

    public Customer customer()
    {
        return (Customer) super.get(CUSTOMER);
    }

    public void populateUserContext()
    {

    }

    public void clearUserContext()
    {

    }

}
