package com.tech.challenge.service;

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 8, 2016        akurmi				Created
*/

/***************************************************************/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tech.challenge.integration.SubscriptionIntegration;
import com.tech.challenge.som.CloudService;
import com.tech.challenge.som.CloudServices;
import com.tech.challenge.som.Order;
import com.tech.challenge.som.Orders;

/***************************************************************/

/**
 * SubscriptionCreatorService
 */
@Service
public class SubscriptionCreatorService
{
    @Autowired
    private SubscriptionIntegration subIntegrate;

    @Autowired
    private UserContext             userContext;

    /**
     * 
     * createSubscription
     *
     * @param services
     * @return
     */
    public Orders createSubscription(CloudServices services)
    {
        Orders orders = new Orders();

        for (CloudService service : services.getCloudServices())
        {
            orders.addOrder(this.cloudServiceToSubscriptionMapping(service));
        }

        return orders;
    }

    /**
     * 
     * callSubscriptionAPI
     *
     * @param orders
     */
    public void callSubscriptionAPI(Orders orders)
    {
        for (Order order : orders.getOrders())
        {
            subIntegrate.createSubscription(this.userContext.getUserId(),
                    this.userContext.customer().getCustomerId(), order);
        }
    }

    /**
     * 
     * cloudServiceToSubscriptionMapping
     *
     * @param service
     * @return
     */
    private Order cloudServiceToSubscriptionMapping(CloudService service)
    {
        Order order = new Order();

        return order;
    }

}
