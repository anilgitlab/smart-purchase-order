package com.tech.challenge.service;

import java.util.Base64;

import org.springframework.stereotype.Service;

/**
 * PasswordEncorderService
 */
@Service
public class PasswordEncorderService
{

    /**
     * 
     * encodePassword
     *
     * @param password
     * @return
     */
    public String encodePassword(String password)
    {
        return Base64.getEncoder().encodeToString(password.getBytes());

    }

    /**
     * 
     * decodePassword
     *
     * @param password
     * @return
     */
    public String decodePassword(String password)
    {
        return new String(Base64.getDecoder().decode(password.getBytes()));

    }

}
