package com.tech.challenge.test;

/*
Date                Author                 	Changes
----------------    -------------------		-------------------
Nov 9, 2016        akurmi				Created
*/

/***************************************************************/
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.tech.challenge.som.User;

/***************************************************************/

/**
 * TestCloudServiceAPI
 */
public class TestCloudServiceAPI
{
    protected RestTemplate rt   = new RestTemplate();

    public final String    BASE = "localhost:8088/smartpurchaseorder";

    public String          authCode;

    public void authenticate()
    {
        User user = new User("admin", "admin123");

        HttpEntity<User> entity = new HttpEntity<User>(user);

        ResponseEntity<String> authCode = rt.exchange(
                BASE + "/api/v1/user/authenticate", HttpMethod.POST, entity,
                String.class);
        this.authCode = authCode.getBody();
        System.out.println(authCode);
    }

    @Test
    public void getAllServices()
    {
        authenticate();
        ResponseEntity<String> json = rt.exchange(BASE + "/api/v1/services",
                HttpMethod.GET, null, String.class);

        System.out.println(json);
    }

    @Test
    public void saveServices()
    {
        authenticate();

        HttpEntity<String> jsonData = new HttpEntity<String>("jsonData");

        ResponseEntity<String> message = rt.exchange(
                BASE + "/api/v1/services/save", HttpMethod.POST, jsonData,
                String.class);
        System.out.println(message);
    }

    @Test
    public void subscribeServices()
    {
        authenticate();

        HttpEntity<String> jsonData = new HttpEntity<String>("jsonData");

        ResponseEntity<String> message = rt.exchange(
                BASE + "/api/v1/services/subscribe", HttpMethod.POST, jsonData,
                String.class);
        System.out.println(message);
    }

}
