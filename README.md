# Smart Purchase Order #

SmartPurchaseOrder is web based application to order different types of cloud services from market place. User from different domain includig non technical users can use visual drag and drop based user interface to order the full stack services.
For example , if user wants to deploy the web application in to Amazon cloud then we know that it require to subscribe may services and install the required softwared like subscribe RHEL operation system , install tomcat web server, install mySQL database, install third party software , enable security like SSL and firewall etc. But by using SmartPurchaseOrder user can drag and drop these services and build abstract service architecture just like given below. user can save for other teammates the same architecture and place order. we have integrated this with AppDirect subscrition API to subscribe these services. 


![visual UI.png](https://bitbucket.org/repo/7q5Eyj/images/591460944-visual%20UI.png)


# Smart Purchase Order Architecture #


![Smart PO.png](https://bitbucket.org/repo/7q5Eyj/images/3640852885-Smart%20PO.png)

### Smart Subscription System  ###

* Visual Purchase Order System is smart ordering system to subscribe the services very simply.
* User can create architecture from list of availabe services by just drag and drop and place an order.
* This system is integrated with AppDirect Subscription API to create purchase order.

### How to setup smartpurchaseorder project ###

* This is spring based web application having very simple rest api.
* take the war file and deploy on popular web server like tomcat
* Dependencies : mysql database and schema.
* Database configuration : edit the smartpo.property file


### Contribution guidelines ###

* Writing tests - JUnit test cases
* Code review - Anil

### Who do I talk to? ###

* Anil Kumar Kurmi
* anil.ku.2012@gmail.com